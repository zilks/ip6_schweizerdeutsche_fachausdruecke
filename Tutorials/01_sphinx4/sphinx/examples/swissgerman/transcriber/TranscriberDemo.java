package sphinx.examples.swissgerman.transcriber;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.SpeechResult;
import edu.cmu.sphinx.api.StreamSpeechRecognizer;

public class TranscriberDemo {

	public static void main(String[] args) throws Exception {
        
        Configuration configuration = new Configuration();

        configuration.setAcousticModelPath("file:voxforge-de-0.1/model_parameters/voxforge_de_sphinx.cd_cont_3000");
        configuration.setDictionaryPath("file:ethergl_speech_ch_de/etc/ethergl_speech_ch_de.dic");
        configuration.setLanguageModelPath("file:voxforge-de-0.1/etc/voxforge_de_sphinx.lm");

        StreamSpeechRecognizer recognizer = new StreamSpeechRecognizer(configuration);
        InputStream stream = new FileInputStream(new File("audio_files_swissgerman/kaufvertrag.wav"));

        recognizer.startRecognition(stream);
        SpeechResult result;
        while ((result = recognizer.getResult()) != null) {
            System.out.format("Hypothesis: %s\n", result.getHypothesis());
        }
        recognizer.stopRecognition();
    }
}
