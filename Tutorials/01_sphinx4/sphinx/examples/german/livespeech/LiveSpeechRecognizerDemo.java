package sphinx.examples.german.livespeech;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.SpeechResult;

public class LiveSpeechRecognizerDemo {

	public static void main(String[] args) throws Exception {
        
        Configuration configuration = new Configuration();

        configuration.setAcousticModelPath("file:voxforge-de-0.1/model_parameters/voxforge_de_sphinx.cd_cont_3000");
        configuration.setDictionaryPath("file:voxforge-de-0.1/etc/voxforge_de_sphinx.dic");
        configuration.setLanguageModelPath("file:voxforge-de-0.1/etc/voxforge_de_sphinx.lm");
        
        configuration.setSampleRate(16000);
        
        LiveSpeechRecognizer recognizer = new LiveSpeechRecognizer(configuration);
        // Start recognition process pruning previously cached data.
        recognizer.startRecognition(true);
        SpeechResult result;
        while ((result = recognizer.getResult()) != null) {
            System.out.format("Hypothesis: %s\n", result.getHypothesis());
        }
        // Pause recognition process. It can be resumed then with startRecognition(false).
        recognizer.stopRecognition();
    }
}
