package sphinx.examples.acousticmodel;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.SpeechResult;

public class AcousticModelTest {

	public static void main(String[] args) throws Exception {
        
        Configuration configuration = new Configuration();

        configuration.setAcousticModelPath("file:voxforge-de-0.1/model_parameters/voxforge_de_sphinx.cd_cont_3000");
        configuration.setDictionaryPath("file:ethergl_speech_ch_de/test.dict");
        configuration.setGrammarPath("file:ethergl_speech_ch_de");
        configuration.setGrammarName("test");	
        configuration.setUseGrammar(true);
        
        LiveSpeechRecognizer recognizer = new LiveSpeechRecognizer(configuration);
        // Start recognition process pruning previously cached data.
        recognizer.startRecognition(true);
        SpeechResult result;
        while ((result = recognizer.getResult()) != null) {
            System.out.format("Hypothesis: %s\n", result.getHypothesis());
        }
        // Pause recognition process. It can be resumed then with startRecognition(false).
        recognizer.stopRecognition();
    }
}
