#!/usr/bin/perl
#Script used to create the prompts: 
#Copyright (C) 2007  Timo Baumann
#This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;

# usage: cat some.words | espeak -v de -x -q 3>&1 1>&2 2>&3 | ./espeak2phones.pl > some.phones

# tokens that have to be transformed from espeak to our phoneset
my %trans = ('3' => 'ei', 'A' => 'a', 'A:', => 'aa:', 'E2' => 'ee', 'i2' => 'i', 
	     'W' => 'ui', 'y' => 'yy', 'Y:' => 'ui:', '*' => 'r', 'C2' => 'cc', 
	     'dZ' => 'd z', 'j/' => 'j', 'l/' => 'l', 'pF' => 'p f', 'tS' => 't ss', 
	     'ts' => 't s', '_!' => 'qq', '_|' => 'qq', '_' => 'sil', 'I2' => 'ii',
	     'aI' => 'ai', 'aU' => 'au', 'E' => 'ee', 'E:' => 'ee:', 
	     'I' => 'ii', '0' => 'oo', 'O' => 'oo', 'OY' => 'oy', 
	     'U' => 'uu','C' => 'cc', 'w' => 'v', 'o' => 'oo',
	     'D' => 'dd', 'T' => 'tt',
	     'S' => 'ss', 'N' => 'nn', 'e' => 'ee',
	    );

# tokens that are identical in espeak and our phoneset
my @keep = ('@', 'a', 'e:', 'i:', 'o:', 
	    'u:', 'y:', 'b',  'd',  'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 
	     'p', 's',  't',  'v', 'x', 'z',  ' ', 
	   );

# tokens that are known output of espeak, but that should not regularly appear
my %error = ('EI' => 1, 'D' => 1, 'r' => 1, 'r/' => 1, 'r-' => 1, 
	    );

# tokens that are known output of espeak and can safely be ignored
my %ignore = ('@-' => 1, ':' => 1, ';' => 1, '_^_' => 1, '\'' => 1, '%%' => 1, 
	      '_' => 1, ',' => 1, 
	     );

my $TRUE = (0 == 0);
my $FALSE = !$TRUE;

my @tokens = (keys %trans, @keep, keys %error, keys %ignore);
@tokens = sort { length $b <=> length $a } @tokens;
#print join "+", @tokens;
#print "\n";
#print $#tokens;

while (my $line = <STDIN>) {
	warn ($line);
	while ($line !~ m/^\s*$/) { # while there's still something to process
		my $token;
		my $i = 0;
		my $match = $FALSE;
		until (($i > $#tokens) or $match) {
			$token = $tokens[$i];
			$match = ($line =~ s/^\Q$token\E//);
			$i++;
		}
		warn "something did not match: $line\n" unless ($match);
		warn "Error case found: $token in $line\n" if ($error{$token});
		next if ($ignore{$token});
		if ($trans{$token}) {
			print "$trans{$token} ";
		} else {
			print "$token "; # this must be a keep token
		}
	}
	print "\n";
}

__END__

3    6
@-   ignore
@    @
a    a
A    a
A:   a:
aI   aI
aU   aU
E    E
E2   E
E:   E:
e:   e:
EI   throw error
I    I
i2   I/i: (i: reduce to I)
i:   i:
O    O
o:   o:
OY   OY
U    U
u:   u:
W    9
y    Y
y:   y:
Y:   2:

*    r
:    ignore
;    ignore
b    b
C    C
C2   if (vowel follows) then /g/ otherwise C
d    d
D    throw error
dZ   d Z
f    f
g    g
h    h
j    j
j/   j
k    k
l    l
l/   l
m    m
n    n
N    N
p    p
pF   p f
r    throw error (upside down R)
r/   throw error (upside down R)
s    s
S    S
t    t
tS   t S
ts   t s
v    v
x    x
z    z
Z    Z
_!   Q
_|   Q
_^_  ignore
'    ignore
%%   ignore

