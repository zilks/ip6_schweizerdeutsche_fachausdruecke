/*
 * Copyright 1999-2004 Carnegie Mellon University.  
 * Portions Copyright 2004 Sun Microsystems, Inc.  
 * Portions Copyright 2004 Mitsubishi Electric Research Laboratories.
 * All Rights Reserved.  Use is subject to license terms.
 */

package ch.fhnw.ether.speech;

public class SpeechSourceProvider {

    Microphone getMicrophone() {
        return new Microphone(16000, 16, true, false);
    }
}
