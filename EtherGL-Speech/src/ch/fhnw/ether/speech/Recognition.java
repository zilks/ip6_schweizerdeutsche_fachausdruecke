package ch.fhnw.ether.speech;

import java.io.File;
import java.io.IOException;

import ch.fhnw.ether.audio.IAudioRenderTarget;
import ch.fhnw.ether.audio.JavaSoundTarget;
import ch.fhnw.ether.audio.URLAudioSource;
import ch.fhnw.ether.audio.fx.AudioGain;
import ch.fhnw.ether.media.AbstractRenderCommand;
import ch.fhnw.ether.media.IScheduler;
import ch.fhnw.ether.media.RenderCommandException;
import ch.fhnw.ether.media.RenderProgram;
import ch.fhnw.ether.ui.ParameterWindow;
import ch.fhnw.ether.ui.ParameterWindow.Flag;


public class Recognition extends AbstractRenderCommand<IAudioRenderTarget> {

	public static void main(String[] args) throws IOException, RenderCommandException {
		
		Configuration configuration = new Configuration();
		
		configuration.setAcousticModelPath("file:voxforge-de-0.1/model_parameters/voxforge_de_sphinx.cd_cont_3000");
        configuration.setDictionaryPath("file:voxforge-de-0.1/etc/voxforge_de_sphinx.dic");
        configuration.setLanguageModelPath("file:voxforge-de-0.1/etc/voxforge_de_sphinx.lm");
		
		LiveSpeechRecognizer recognizer = new LiveSpeechRecognizer(configuration);
		
		//Start recognition process pruning previously cached data.
		recognizer.startRecognition(true);
		SpeechResult result;
		while ((result = recognizer.getResult()) != null) {
            System.out.format("Hypothesis: %s\n", result.getHypothesis());
        }
		//Pause recognition process. It can be resumed then with startRecognition(false).
		recognizer.stopRecognition();
		
		URLAudioSource track = new URLAudioSource(new File(args[0]).toURI().toURL());
		AudioGain gain = new AudioGain();
		FrontEndProcessing processing = new FrontEndProcessing();
		RenderProgram<IAudioRenderTarget> program = new RenderProgram<>(track, gain, processing);
		
		new ParameterWindow(program, Flag.EXIT_ON_CLOSE);
		
		JavaSoundTarget audioOut = new JavaSoundTarget();
		audioOut.useProgram(program);
		audioOut.start();
		audioOut.sleepUntil(IScheduler.NOT_RENDERING);
		audioOut.stop();
	}

	@Override
	protected void run(IAudioRenderTarget target) throws RenderCommandException {
		// TODO Auto-generated method stub
	}
	
}
