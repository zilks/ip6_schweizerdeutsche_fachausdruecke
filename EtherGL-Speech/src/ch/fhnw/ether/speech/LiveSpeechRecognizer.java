/*
 * Copyright 2013 Carnegie Mellon University.
 * Portions Copyright 2004 Sun Microsystems, Inc.
 * Portions Copyright 2004 Mitsubishi Electric Research Laboratories.
 * All Rights Reserved.  Use is subject to license terms.
 */

package ch.fhnw.ether.speech;

import java.io.IOException;

import edu.cmu.sphinx.frontend.util.StreamDataSource;

public class LiveSpeechRecognizer extends AbstractSpeechRecognizer {
	
	private final Microphone microphone;
	
	//Constructs new live recognition object
	public LiveSpeechRecognizer(Configuration configuration) throws IOException
    {
        super(configuration);
        microphone = speechSourceProvider.getMicrophone();
        context.getInstance(StreamDataSource.class).setInputStream(microphone.getStream());
    }
	
	//Starts recognition
	public void startRecognition(boolean clear) {
        recognizer.allocate();
        microphone.startRecording();
    }
	
	//Stops recognition
	public void stopRecognition() {
        microphone.stopRecording();
        recognizer.deallocate();
    }

}
